import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:imd_weather/values/MyColors.dart';
import 'package:imd_weather/values/MyTextStyles.dart';
import 'package:imd_weather/dialogs/retry_internet.dart';

class NoInternet extends StatefulWidget {
  @override
  _NoInternetState createState() => _NoInternetState();
}

class _NoInternetState extends State<NoInternet> {
  bool internet = true;
  _checkInternet() async {
    var result = await Connectivity().checkConnectivity();
    if (result == ConnectivityResult.none) {
//      showAlertDialog(context);
      setState(() {
        internet = false;
      });
    } else {
      setState(() {
        internet = true;
      });
      Navigator.pop(context);
    }
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Center(
        child: Column(
          children: <Widget>[
            SizedBox(height: 130),
            Container(
              width: 200,
              height: 250,
              child: Image(
                image: AssetImage("assets/images/noconnection/nointernet.jpg"),
              ),
            ),
            SizedBox(height: 17),
            Text(
              "Oh-Oh!",
              style: MyTextStyles.Whoops,
            ),
            SizedBox(height: 15),
            Text(
              "No internet connection found.",
              style: MyTextStyles.noInternetText,
            ), SizedBox(
              height: 5,
            ),
            Text(
              "Check your connection and try again",
              style: MyTextStyles.noInternetText,
            ),
            SizedBox(
              height: 45,
            ),
            Container(
              width: 170,
              child: RaisedButton(
                color: MyColors.noInternetbutton,
                onPressed: ()async {
                  await Future.delayed(Duration(seconds: 1));
                  await _checkInternet();
                  if(!internet) {
                    showDialog(context: context,
                        builder: (context) => retry_internet());
                  }
                },
                child: Text(
                  "RETRY",
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 17.0,
                      fontWeight: FontWeight.bold,
                      letterSpacing: 2),
                ),
                shape: RoundedRectangleBorder(
                  borderRadius: new BorderRadius.circular(18.0),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

//  void showAlertDialog(BuildContext context) {
//
//    Widget okButton = FlatButton(
//      child: Text("Ok"),
//      onPressed: () {
//
//        },
//    );
//
//    // Create AlertDialog
//    Dialog alert = Dialog(
//      shape: RoundedRectangleBorder(
//        borderRadius: BorderRadius.circular(16)
//      ),
//      elevation: 0,
//      backgroundColor: Colors.transparent,
//      child: _buildChild(context),
//
//    );
//    _buildChild(BuildContext context) => Container(
//
//    );
//
//    // show the dialog
//    showDialog(
//      context: context,
//      builder: (BuildContext context) {
//        return alert;
//      },
//    );
//  }
}
